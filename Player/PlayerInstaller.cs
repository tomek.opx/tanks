using UnityEngine;
using UnityEngine.UI;
using Zenject;
namespace Zenject.Tanks
{ 
public class PlayerInstaller : MonoInstaller
{
    [SerializeField]
    private int _baseHealth = 100;
    [SerializeField]
    private Text _healthText;
    public override void InstallBindings()
    {
        Container.BindInstance(_healthText);
        Container.BindInstance(_baseHealth);
        Container.BindInterfacesAndSelfTo<Player>().AsSingle();
        Container.BindInterfacesAndSelfTo<PlayerUI>().AsSingle();
    }
}
}