using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Zenject.Tanks
{ 
public class Player : IInitializable
{
        private int _currentHealth;
        private int _baseHealth;
        private PlayerUI _playerUI;

        public Player(int baseHealth, PlayerUI playerUI)
        {
            _baseHealth = baseHealth;
            _playerUI = playerUI;
        }
        public void Initialize()
        {
            _currentHealth = _baseHealth;
            _playerUI.SetHealth(_currentHealth);
        }
        public void TakeDamage(int damage)
        {
            _currentHealth -= damage;
            _playerUI.SetHealth(_currentHealth);
        }
}
}
