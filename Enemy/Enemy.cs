using UnityEngine;
namespace Zenject.Tanks
{ 
public class Enemy : ITickable
    {
        public enum Difficulty
        {
            Easy, Medium, Hard
        }


        private Difficulty _difficulty;

        public Enemy(Difficulty difficulty)
        {
            _difficulty = difficulty;
        }
        public void Tick()
        {
            Debug.Log(_difficulty);
        }
}
}