using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Zenject.Tanks
{
    public class PlayerInputState
    {
        public bool IsMovingLeft
        {
            get;
            set;
        }
        public bool IsMovingRight
        {
            get;
            set;
        }
        public bool IsMovingUp
        {
            get;
            set;
        }
        public bool IsMovingDown
        {
            get;
            set;
        }
        public bool IsFiring
        {
            get;
            set;
        }
    }
}
