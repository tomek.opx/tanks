using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Zenject.Tanks
{
    public class PlayerUI 
{
        private Text _healthText;

        public PlayerUI(Text healthText)
        {
            _healthText = healthText;
        }
        public void SetHealth(int health)
        {
            _healthText.text = health.ToString();
        }
}
}
